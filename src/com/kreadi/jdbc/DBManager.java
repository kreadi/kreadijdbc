package com.kreadi.jdbc;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.dbcp.BasicDataSource;

/**
 * Manager de conexion JDBC KREADI: Pool de conexiones , Definicion de preparedstatements en SQL externo, Metodos de acceso simples
 *
 * Ejemplo de archivo SQL:
 *
 * # getMaxId(1) = SELECT max(id) FROM map WHERE base=?;
 *
 * Debe agregar la variable publica getMaxId la clase que extiende DBManager... el metodo usara la conexion 1... por defecto si no pone parentesis usa la conexion 0
 */
public abstract class DBManager {

    private BasicDataSource dataSource;
    private final HashMap<String, String> scriptMap;
    private final HashMap<String, Integer> connMap;
    private final HashMap<String, String[]> keyMap;

    /**
     * Define la conexion con una base de datos segun los parametros especificados, busca en un fichero sql de mismo nombre que la classe las sentencias SQL
     *
     * @param driver
     * @param user
     * @param pass
     * @param url
     * @param props
     * @param log
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    public DBManager(String driver, String user, String pass, String[] url, String props, boolean log, int tries) throws IllegalArgumentException, IllegalAccessException, SQLException {
        scriptMap = new HashMap<>();
        connMap = new HashMap<>();
        keyMap = new HashMap<>();
        int bufferSize = 2048;
        byte[] buffer = new byte[bufferSize];
        StringBuilder sb = new StringBuilder();
        int read;
        try (InputStream is = getClass().getResource(getClass().getSimpleName() + ".sql").openStream()) {
            while ((read = is.read(buffer)) != -1) {
                sb.append(new String(buffer, 0, read));
            }
        } catch (IOException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String scripts = sb.toString();
        Pattern patter = Pattern.compile("\\s*#\\s*\\w+\\s*(\\(\\d+\\))?\\s*(\\[\\w+(,\\w+)*\\])?\\s*=\\s*\n");
        Matcher matcher = patter.matcher(scripts);

        String newname, name = null, sql;
        int newIdx, connIdx = 0;
        int begin = 0, end;
        int conns = 0;
        String[] keys = null, newKeys;
        while (matcher.find()) {
            newname = matcher.group().trim().replaceAll("[ \t\n\f\r]", "");
            newname = newname.substring(1, newname.length() - 1);
            newIdx = 0;
            newKeys = null;
            if (newname.endsWith("]")) {
                int idxKey = newname.indexOf("[");
                newKeys = newname.substring(idxKey + 1, newname.length() - 1).split(",");
                newname = newname.substring(0, idxKey);
            }
            if (newname.endsWith(")")) {
                int pos = newname.indexOf("(");
                newIdx = Integer.parseInt(newname.substring(pos + 1, newname.length() - 1));
                newname = newname.substring(0, pos);
            }
            if (name != null) {
                end = matcher.start();
                sql = scripts.substring(begin, end).trim();
                scriptMap.put(name, sql);
                connMap.put(name, connIdx);
                keyMap.put(name, keys);
                if (connIdx > conns) {
                    conns = connIdx;
                }
            }
            name = newname;
            connIdx = newIdx;
            keys = newKeys;
            begin = matcher.end();
        }
        if (name != null) {
            end = scripts.length();
            sql = scripts.substring(begin, end).trim();
            scriptMap.put(name, sql);
            connMap.put(name, connIdx);
            keyMap.put(name, keys);
            if (connIdx > conns) {
                conns = connIdx;
            }
        }

        for (String u : url) {
            System.out.println("                ------------------------------\n                Conectando con "+u);
            dataSource = new BasicDataSource();
            dataSource.setDriverClassName(driver);
            dataSource.setUsername(user);
            dataSource.setPassword(pass);
            dataSource.setUrl(u);
            if (props != null) {
                dataSource.setConnectionProperties(props);
            }
        }
        Connection[] conexion = new Connection[conns + 1];
        int tryCount = 0;
        for (int i = 0; i <= conns; i++) {
            try {
                conexion[i] = getConnection();
            } catch (Exception e) {
                tryCount++;
                if (tryCount < tries) {
                    i--;
                    System.err.println("No se puede conectar con la DB, proximo intento en 10 seg.\n" + e.getMessage());
                    try {
                        Thread.sleep(10000);
                    } catch (Exception ex) {
                    }
                }
            }
        }
        for (Field field : getClass().getDeclaredFields()) {
            if (field.getType().equals(SQL.class)) {
                String nam = field.getName();
                if (!nam.startsWith("_")) {
                    try {
                        Integer i = connMap.get(nam);
                        String[] k = keyMap.get(nam);
                        if (i != null) {
                            SQL loadSql = getSQL(nam, conexion[i], k);
                            if (log) {
                                loadSql.log(log);
                            }
                            field.setAccessible(true);
                            field.set(this, loadSql);
                            if (log) {
                                System.out.println(nam + " -> " + scriptMap.get(nam));
                            }
                        } else {
                            System.err.println(nam + " not found in " + getClass().getSimpleName() + ".sql");
                        }
                    } catch (IllegalAccessException | IllegalArgumentException | SQLException e) {
                        System.err.println(nam + ": " + e.getMessage());
                    }
                }
            }
        }
    }

    /**
     * Define la conexion con una base de datos segun los parametros especificados, busca en un fichero sql de mismo nombre que la classe las sentencias SQL
     *
     * @param driver
     * @param user
     * @param pass
     * @param tries
     * @param url
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    public DBManager(String driver, String user, String pass, int tries, String... url) throws IllegalArgumentException, IllegalAccessException, SQLException {
        this(driver, user, pass, url, null, false, tries);
    }

    public void dropPostgresTables() throws SQLException {
        LinkedList<String> tables = new LinkedList<>();
        try (Statement stmt = getStatement()) {
            do {
                tables.clear();
                try (ResultSet rs = stmt.executeQuery("SELECT table_name FROM information_schema.tables where table_schema='public'")) {
                    while (rs.next()) {
                        tables.add(rs.getString(1));
                    }
                }
                if (!tables.isEmpty()) {
                    for (String table : tables) {
                        try {
                            stmt.execute("DROP TABLE " + table + ";");
                        } catch (Exception e) {
                            String msg = e.getMessage();
                            if (msg.contains("Use DROP")) {
                            } else {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            } while (!tables.isEmpty());
        }
    }

    /**
     * Cierra de dataSource
     *
     * @throws java.sql.SQLException
     */
    public void close() throws SQLException {
        dataSource.close();
    }

    public boolean isClosed() {
        return dataSource.isClosed();
    }

    /**
     * Retorna la cantidad de conexiones activas
     *
     * @return
     */
    public int getConnections() {
        return dataSource.getNumActive();
    }

    /**
     * Ejecuta una sentencia sql, especificando una conexion y libera recursos usados y retorna una lista de array de objetos
     *
     * @param sql
     * @param con
     * @return
     * @throws java.sql.SQLException
     */
    public List<Object[]> executeQueryList(String sql, Connection con) throws SQLException {
        Statement stmt = getStatement(con);
        ResultSet rs = stmt.executeQuery(sql);
        int cols = rs.getMetaData().getColumnCount();
        Object[] obj;
        LinkedList<Object[]> list = new LinkedList<>();
        while (rs.next()) {
            obj = new Object[cols];
            for (int i = 0; i < cols; i++) {
                obj[i] = rs.getObject(i + 1);
            }
            list.add(obj);
        }
        if (con == null) {
            closeResultSetAndStatement(rs);
        } else {
            closeResultSetAndStatement(rs, false);
        }
        return list;
    }

    /**
     * Ejecuta una sentencia sql y libera recursos usados y retorna una lista de array de objetos
     *
     * @param sql
     * @return
     * @throws java.sql.SQLException
     */
    public List<Object[]> executeQueryList(String sql) throws SQLException {
        return executeQueryList(sql, null);
    }

    public Object getObject(String key) throws FileNotFoundException, IOException, ClassNotFoundException {
        Object obj;
        try (FileInputStream fis = new FileInputStream(key + ".obj"); ObjectInputStream ois = new ObjectInputStream(fis)) {
            obj = ois.readObject();
        }
        return obj;
    }

    public void setObject(String key, Serializable object) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(key + ".obj"); ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(object);
        }
    }

    public String dv(int valor) {
        int M = 0, S = 1, T = valor;
        for (; T != 0; T /= 10) {
            S = (S + T % 10 * (9 - M++ % 6)) % 11;
        }
        return "" + (char) (S != 0 ? S + 47 : 75);
    }

    public final SQL getSQL(String sqlName, Connection conn, String[] keys) throws SQLException {
        String sql = scriptMap.get(sqlName);
        if (sql == null) {
            System.err.println("No existe el sql para: " + sqlName);
            return null;
        }
        return new SQL(sqlName, sql, conn, keys);
    }

    public final Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public int executeUpdate(String sql) throws SQLException {
        try (Connection conn = getConnection(); Statement stmt = conn.createStatement()) {
            int result = stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
            return result;
        }
    }

    public ResultSet executeQuery(String sql, int maxSeconds) throws SQLException {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        stmt.setQueryTimeout(maxSeconds);
        return stmt.executeQuery(sql);
    }

    public void closeStatement(Statement stmt) throws SQLException {
        try (Connection conn = stmt.getConnection()) {
            stmt.close();
            conn.close();
        }
    }

    public void closeResultSetAndStatement(ResultSet rs) throws SQLException {
        closeResultSetAndStatement(rs, true);
    }

    public void closeResultSetAndStatement(ResultSet rs, boolean closeConn) throws SQLException {
        Connection conn;
        try (Statement stmt = rs.getStatement()) {
            rs.close();
            conn = stmt.getConnection();
        }
        if (closeConn) {
            conn.close();
        }
    }

    public ResultSet executeQuery(String sql) throws SQLException {
        return executeQuery(sql, null);
    }

    public ResultSet executeQuery(String sql, Connection con) throws SQLException {
        Statement stmt = getStatement(con);
        return stmt.executeQuery(sql);
    }

    public Statement getStatement() throws SQLException {
        return getStatement(null);
    }

    public Statement getStatement(Connection conn) throws SQLException {
        if (conn == null) {
            conn = getConnection();
        }
        return conn.createStatement();
    }

    public static float wordDistance(String s1, String s2) {
        if (s1 == null || s1.isEmpty()) {
            if (s2 == null || s2.isEmpty()) {
                return 0;
            } else {
                return s2.length();
            }
        }
        if (s2 == null || s2.isEmpty()) {
            return s1.length();
        }

        int i, c = 0;
        int lcs = 0;
        int maxOffset = 5;
        int offset1 = 0;
        int offset2 = 0;

        final int s1Length = s1.length();
        final int s2Length = s2.length();

        while ((c + offset1 < s1Length) && (c + offset2 < s2Length)) {
            if (s1.charAt(c + offset1) == s2.charAt(c + offset2)) {
                lcs++;
            } else {
                offset1 = offset2 = i = 0;
                while (i < maxOffset) {
                    if ((c + i < s1Length) && (s1.charAt(c + i) == s2.charAt(c))) {
                        offset1 = i;
                        break;
                    }
                    if ((c + i < s2Length) && (s1.charAt(c) == s2.charAt(c + i))) {
                        offset2 = i;
                        break;
                    }
                    i++;
                }
            }
            c++;
        }
        return (s1Length + s2Length) / 2f - lcs;
    }
}
