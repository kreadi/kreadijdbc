package com.kreadi.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SQL {

    public static boolean alllog = false;

    private PreparedStatement pstmt = null;
    private final Connection conn;
    private final String sql;
    private final String name;
    private final boolean returnKeys = false;
    private boolean log = false;

    public SQL log(boolean log) {
        this.log = log;
        return this;
    }

    public void setAutoCommit(boolean auto) throws SQLException {
        getConnection().setAutoCommit(auto);
    }

    public void commit() throws SQLException {
        getConnection().commit();
    }

    public void rollback() throws SQLException {
        getConnection().rollback();
    }

    public void rollback(Savepoint sp) throws SQLException {
        getConnection().rollback(sp);
    }

    public Connection getConnection() throws SQLException {
        return conn;
    }

    public SQL(String name, String sql, Connection conn, String[] returnKeys) throws SQLException {
        this.name = name;
        this.sql = sql != null ? sql.replaceAll("//.*|(\"(?:\\\\[^\"]|\\\\\"|.)*?\")|(?s)/\\*.*?\\*/", "$1 ") : null;
        this.conn = conn;
    }

    private void setParams(Object... params) throws SQLException {
        if (pstmt == null) {
            pstmt = conn.prepareStatement(sql, returnKeys ? Statement.RETURN_GENERATED_KEYS : Statement.NO_GENERATED_KEYS);
        }
        for (int i = 0; i < params.length; i++) {
            Object o = params[i];
            if (o instanceof Timestamp) {
                pstmt.setTimestamp(i + 1, (Timestamp) o);
            } else {
                pstmt.setObject(i + 1, o);
            }
        }
    }

    public boolean getMoreResults() throws SQLException {
        return pstmt.getMoreResults();
    }

    public int getUpdateCount() throws SQLException {
        return pstmt.getUpdateCount();
    }

    /**
     * Retorna un resultset desde una operacion execute previa;
     *
     * @return
     * @throws SQLException
     */
    public ResultSet getResultSet() throws SQLException {
        return pstmt.getResultSet();
    }

    public boolean execute(Object... params) throws SQLException {
        setParams(params);
        if (alllog || log) {
            System.out.println(name + "> EXECUTE > " + pstmt);
        }
        try {
            return pstmt.execute();
        } catch (SQLException e) {
            throw e;
        }
    }

    public List<Integer> getLastIDS() throws SQLException {
        LinkedList<Integer> list;
        try (ResultSet rs = pstmt.getGeneratedKeys()) {
            list = new LinkedList<>();
            if (rs.next()) {
                list.add(rs.getInt(1));
                while (rs.next()) {
                    list.add(rs.getInt(1));
                }
                return list;
            } else {
                return null;
            }
        }
    }

    public int[] executeBatch(Object... params) throws SQLException {
        if (params == null) {
            params = new Object[]{null};
        }
        setParams(params);
        if (alllog || log) {
            System.out.println(name + "> EXECUTEBATCH > " + pstmt);
        }
        try {
            return pstmt.executeBatch();
        } catch (SQLException e) {
            throw e;
        }
    }

    public int executeUpdate(Object... params) throws SQLException {
        if (params == null) {
            params = new Object[]{null};
        }
        setParams(params);
        if (alllog || log) {
            System.out.println(name + "> EXECUTEUPDATE > " + pstmt);
        }
        try {
            return pstmt.executeUpdate();
        } catch (SQLException e) {
            throw e;
        }
    }

    public void addBatch(Object... params) throws SQLException {
        setParams(params);
        if (alllog || log) {
            System.out.println(name + "> ADDBATCH > " + pstmt);
        }
        try {
            pstmt.addBatch();
        } catch (SQLException e) {
            throw e;
        }
    }

    public int[] executeBatch() throws SQLException {
        pstmt.clearParameters();
        try {
            return pstmt.executeBatch();
        } catch (SQLException e) {
            throw e;
        }

    }

    public Object[] executeFirstQuery(Object... params) throws SQLException {
        //long init = System.currentTimeMillis();

        setParams(params);
        if (alllog || log) {
            System.out.println(name + "> " + pstmt);
        }

        try (ResultSet rs = pstmt.executeQuery()) {
            int cols = rs.getMetaData().getColumnCount();
            Object[] obj;
            if (rs.next()) {
                obj = new Object[cols];
                for (int i = 0; i < cols; i++) {
                    obj[i] = rs.getObject(i + 1);
                }
                rs.close();
                //long end = System.currentTimeMillis();
                //System.out.println(">> ," + (end - init) + "," + name);
                return obj;
            }
            rs.close();
            return null;
        }
    }

    public ResultSet executeResultSet(Object... params) throws SQLException {
        setParams(params);
        if (alllog || log) {
            System.out.println(name + "> " + pstmt);
        }
        return pstmt.executeQuery();
    }

    public List<Object[]> executeQuery(Object... params) throws SQLException {
        ArrayList<Object[]> result = new ArrayList<>();
        setParams(params);
        if (alllog || log) {
            System.out.println(name + "> " + pstmt);
        }
        try (ResultSet rs = pstmt.executeQuery()) {
            int cols = rs.getMetaData().getColumnCount();
            Object[] obj;
            while (rs.next()) {
                obj = new Object[cols];
                for (int i = 0; i < cols; i++) {
                    obj[i] = rs.getObject(i + 1);
                }
                result.add(obj);
            }
            rs.close();
        } catch (Exception e) {
            throw e;
        }
        return result;
    }
}
